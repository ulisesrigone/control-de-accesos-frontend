import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EspaciosComponent } from './espacios/espacios.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EspacioService } from './services/espacio.service';
import { EspacioHorariosComponent } from './espacios/espacio-horarios/espacio-horarios.component';
import { EspacioPermisosComponent } from './espacios/espacio-permisos/espacio-permisos.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { CrearEspacioComponent } from './espacios/crear-espacio/crear-espacio.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from 'src/environments/environment';
import { CrearHorarioComponent } from './espacios/espacio-horarios/crear-horario/crear-horario.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { CrearPermisoComponent } from './espacios/espacio-permisos/crear-permiso/crear-permiso.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { EspacioIngresosComponent } from './espacios/espacio-ingresos/espacio-ingresos.component';
import { MatPaginatorModule } from '@angular/material/paginator';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    EspaciosComponent,
    EspacioHorariosComponent,
    EspacioPermisosComponent,
    CrearEspacioComponent,
    NavbarComponent,
    CrearHorarioComponent,
    CrearPermisoComponent,
    EspacioIngresosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatDialogModule,
    FormsModule,
    MatInputModule,
    MatButtonModule,
    MatDialogModule,
    MatTableModule,
    MatSelectModule,
    MatCheckboxModule,
    MatFormFieldModule,
    BrowserModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatAutocompleteModule,
    MatPaginatorModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    })
  ],
  providers: [EspacioService],
  bootstrap: [AppComponent],
  entryComponents: [CrearEspacioComponent, CrearHorarioComponent]

})
export class AppModule { }
