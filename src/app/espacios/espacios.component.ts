import { Component, OnInit } from '@angular/core';
import { Espacio } from '../model/espacio';
import { EspacioService } from '../services/espacio.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { CrearEspacioComponent } from './crear-espacio/crear-espacio.component';

@Component({
  selector: 'app-espacios',
  templateUrl: './espacios.component.html',
  styleUrls: ['./espacios.component.css']
})
export class EspaciosComponent implements OnInit {
 
  espacios: Espacio[];
  espacio: Espacio;
 
  constructor(private espacioService: EspacioService, private router: Router, public dialog: MatDialog) {
  }
 
  ngOnInit() {
    this.espacioService.findAll()
      .subscribe(data => {
        this.espacios = data;
    });
  }

  gestionarHorarios(idEspacio: number) {
    this.router.navigate(['espacio', idEspacio, 'horarios']);
  }

  gestionarPermisos(idEspacio: number) {
    this.router.navigate(['espacio', idEspacio, 'permisos']);
  }

  historialIngresos(idEspacio: number) {
    this.router.navigate(['espacio', idEspacio, 'ingresos']);
  }

  openDialog(): void {
    this.espacio = new Espacio;
    const dialogRef = this.dialog.open(CrearEspacioComponent, {
      width: '250px',
      data: {espacio: this.espacio}
    });
    dialogRef.afterClosed().subscribe(resultado => {
      this.espacio = resultado;
      this.espacioService.crearEspacio(this.espacio).subscribe(resultado => this.router.navigate(['/espacios']));
    });
  }

  eliminarEspacio(id: number) {
    this.espacioService.deleteEspacio(id).subscribe(resultado => this.router.navigate(['/espacios']));
  }

}