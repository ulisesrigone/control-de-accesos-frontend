import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Espacio } from '../../model/espacio';

@Component({
  selector: 'app-crear-espacio',
  templateUrl: './crear-espacio.component.html',
  styleUrls: ['./crear-espacio.component.css']
})
export class CrearEspacioComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<CrearEspacioComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Espacio) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {}

}