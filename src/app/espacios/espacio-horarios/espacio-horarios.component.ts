import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EspacioService } from '../../services/espacio.service';
import { Espacio } from '../../model/espacio';
import { Momento } from '../../model/momento';
import { MatDialog } from '@angular/material/dialog';
import { CrearHorarioComponent } from './crear-horario/crear-horario.component';
import { VinculoMomentoEspacio } from 'src/app/model/vinculo-momento-espacio';

@Component({
  selector: 'app-espacio-horarios',
  templateUrl: './espacio-horarios.component.html',
  styleUrls: ['./espacio-horarios.component.css']
})
export class EspacioHorariosComponent implements OnInit {
  
  horarios: VinculoMomentoEspacio[];
  idEspacio: number;
  espacio: Espacio;
  horario: Momento;

  constructor(
    private activatedRoute: ActivatedRoute, 
    private espacioService: EspacioService,
    public dialog: MatDialog,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.idEspacio = parseInt(this.activatedRoute.snapshot.paramMap.get('id'));
    this.espacio = new Espacio;
    this.espacioService.getEspacio(this.idEspacio)
      .subscribe(resultado => {
        this.espacio = resultado
      });
    this.espacioService.getHorariosByEspacio(this.idEspacio)
      .subscribe(resultado => {
        this.horarios = resultado;
      });
  }
  onSubmit() {
  }

  crearHorarioDialog(): void {
    this.horario = new Momento;
    const dialogRef = this.dialog.open(CrearHorarioComponent, {
      width: '250px',
      data: {horario: this.horario}
    });
    dialogRef.afterClosed().subscribe(resultado => {
      this.horario = resultado;
      this.horario.activo = true;
      this.espacioService.crearHorario(this.idEspacio,this.horario).subscribe(resultado => 
        this.router.navigate([`/espacio/${this.idEspacio}/horarios`]));
    });
  }

  agregar(horario: Momento) {
    this.espacioService.crearHorario(this.idEspacio,horario);
  }

}