import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Momento } from '../../../model/momento';

@Component({
  selector: 'app-crear-horario',
  templateUrl: './crear-horario.component.html',
  styleUrls: ['./crear-horario.component.css']
})
export class CrearHorarioComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<CrearHorarioComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Momento) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {}

}
