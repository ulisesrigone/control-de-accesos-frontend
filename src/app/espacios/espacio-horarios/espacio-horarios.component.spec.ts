import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EspacioHorariosComponent } from './espacio-horarios.component';

describe('EspacioHorariosComponent', () => {
  let component: EspacioHorariosComponent;
  let fixture: ComponentFixture<EspacioHorariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EspacioHorariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EspacioHorariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
