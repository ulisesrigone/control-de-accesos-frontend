import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EspacioIngresosComponent } from './espacio-ingresos.component';

describe('EspacioIngresosComponent', () => {
  let component: EspacioIngresosComponent;
  let fixture: ComponentFixture<EspacioIngresosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EspacioIngresosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EspacioIngresosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
