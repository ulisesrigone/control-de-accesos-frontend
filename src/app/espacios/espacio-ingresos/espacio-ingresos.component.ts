import { Component, OnInit } from '@angular/core';
import { Espacio } from '../../model/espacio';
import { Ingreso } from '../../model/ingreso';
import { ActivatedRoute } from '@angular/router';
import { EspacioService } from '../../services/espacio.service';

@Component({
  selector: 'app-espacio-ingresos',
  templateUrl: './espacio-ingresos.component.html',
  styleUrls: ['./espacio-ingresos.component.css']
})
export class EspacioIngresosComponent implements OnInit {

  idEspacio: number;
  espacio: Espacio;
  ingresos: Ingreso[];

  constructor(private activatedRoute: ActivatedRoute, private espacioService: EspacioService) { }

  ngOnInit(): void {
    this.idEspacio = parseInt(this.activatedRoute.snapshot.paramMap.get('id'));
    this.espacio = new Espacio;
    this.espacioService.getEspacio(this.idEspacio)
      .subscribe(resultado => {
        this.espacio = resultado
      });
    this.espacioService.getIngresosByEspacio(this.idEspacio)
      .subscribe(resultado => {
        this.ingresos = resultado;
      });
  }

}
