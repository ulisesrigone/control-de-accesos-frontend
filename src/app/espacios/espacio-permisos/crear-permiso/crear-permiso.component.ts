import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Persona } from 'src/app/model/persona';
import { Momento } from 'src/app/model/momento';
import { Espacio } from 'src/app/model/espacio';
import { EspacioService } from 'src/app/services/espacio.service';
import { PersonaService } from 'src/app/services/persona.service';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { VinculoMomentoEspacio } from 'src/app/model/vinculo-momento-espacio';

export interface DialogData {
  espacio: Espacio;
  persona: Persona;
  vinculoMomentoEspacio: VinculoMomentoEspacio;
}

@Component({
  selector: 'app-crear-permiso',
  templateUrl: './crear-permiso.component.html',
  styleUrls: ['./crear-permiso.component.css'],
  providers: [PersonaService]
})
export class CrearPermisoComponent implements OnInit {

  horarios: VinculoMomentoEspacio[];
  personas: Persona[];
  myControl = new FormControl();
  filteredOptions: Observable<Persona[]>;

  constructor(
    public dialogRef: MatDialogRef<CrearPermisoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private espacioService: EspacioService,
    private personaService: PersonaService) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.espacioService.getHorariosByEspacio(this.data.espacio.id).subscribe(resultado => {
      this.horarios = resultado;
    });
    this.personaService.findAll().subscribe(resultado => {
      this.personas = resultado;
      this.filteredOptions = this.myControl.valueChanges
        .pipe(
          startWith(''),
          map(value => typeof value === 'string' ? value : value.name),
          map(name => name ? this._filter(name) : this.personas.slice())
      );
    });
  }

  displayFn(persona: Persona): string {
    return persona && persona.nombre ? persona.nombre : '';
  }

  private _filter(name: string): Persona[] {
    const filterValue = name.toLowerCase();
    return this.personas.filter(option => option.nombre.toLowerCase().indexOf(filterValue) === 0);
  }

}
