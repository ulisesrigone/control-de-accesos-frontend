import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EspacioPermisosComponent } from './espacio-permisos.component';

describe('EspacioPermisosComponent', () => {
  let component: EspacioPermisosComponent;
  let fixture: ComponentFixture<EspacioPermisosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EspacioPermisosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EspacioPermisosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
