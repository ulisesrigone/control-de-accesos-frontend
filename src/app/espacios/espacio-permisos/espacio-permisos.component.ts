import { Component, OnInit, ViewChild } from '@angular/core';
import { PermisoService } from '../../services/permiso.service';
import { EspacioService } from '../../services/espacio.service';
import { Permiso } from '../../model/permiso';
import { Espacio } from '../../model/espacio';
import { SolicitudPermiso } from '../../model/solicitud-permiso';
import { ActivatedRoute, Router } from '@angular/router';
import { CrearPermisoComponent } from './crear-permiso/crear-permiso.component';
import { MatDialog } from '@angular/material/dialog';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-espacio-permisos',
  templateUrl: './espacio-permisos.component.html',
  styleUrls: ['./espacio-permisos.component.css']
})
export class EspacioPermisosComponent implements OnInit {
  permisos: Permiso[];
  permiso: Permiso;
  idEspacio: number;
  espacio: Espacio;
  solicitudes: SolicitudPermiso[];
  dataSource: MatTableDataSource<SolicitudPermiso>;

  displayedColumns: string[] = ['persona', 'dia', 'franja', 'fecha', 'acciones'];

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private actRoute: ActivatedRoute, 
    private permisoService: PermisoService, 
    private espacioService: EspacioService,
    public dialog: MatDialog,
    private router: Router
    ) { } 

  ngOnInit(): void {
    this.idEspacio = parseInt(this.actRoute.snapshot.paramMap.get('id'));
    this.espacio = new Espacio;
    this.espacioService.getEspacio(this.idEspacio)
      .subscribe(resultado => {
        this.espacio = resultado
      });
    this.permisoService.getPermisosByEspacio(this.idEspacio)
      .subscribe(data => { 
        this.permisos = data; 
      });
    this.permisoService.getSolicitudesByEspacio(this.idEspacio)
      .subscribe(data => {
        this.solicitudes = data;
        this.dataSource = new MatTableDataSource(this.solicitudes);
        this.dataSource.sort = this.sort;
      });
  }

    openDialog(): void {
    this.permiso = new Permiso;
    const dialogRef = this.dialog.open(CrearPermisoComponent, {
        data: {espacio: this.espacio}
    });
    dialogRef.afterClosed().subscribe(resultado => {
      this.permiso = resultado;
      this.permiso.activo = true;
      this.permisoService.crearPermiso(this.permiso).subscribe();
    });
  }

  aceptarSolicitud(solicitudPermiso: SolicitudPermiso) {
    solicitudPermiso.aceptado=1;
    this.permisoService.updateSolicitudPermiso(solicitudPermiso).subscribe();
  }

  rechazarSolicitud(solicitudPermiso: SolicitudPermiso) {
    solicitudPermiso.aceptado=2;
    this.permisoService.updateSolicitudPermiso(solicitudPermiso).subscribe();
  }

}
