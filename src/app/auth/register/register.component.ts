import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [AuthService]
})
export class RegisterComponent implements OnInit {
  
  registerForm: FormGroup;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.registerForm = new FormGroup({
      email: new FormControl(''),
      password: new FormControl(''),
    })
  }

  async onRegister(){
    const {email, password} = this.registerForm.value;
    try {
      const user = await this.authService.register(email, password);
      if (user) {
        this.router.navigate(['/espacios']);
      }
    } 
    catch(error){console.log(error)}
    this.authService.login(email, password);
  }
 
}
