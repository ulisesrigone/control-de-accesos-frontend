import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EspaciosComponent } from './espacios/espacios.component';
import { EspacioPermisosComponent } from './espacios/espacio-permisos/espacio-permisos.component';
import { EspacioHorariosComponent } from './espacios/espacio-horarios/espacio-horarios.component';
import { EspacioIngresosComponent } from './espacios/espacio-ingresos/espacio-ingresos.component';

const routes: Routes = [
  { path: '', redirectTo: 'espacios', pathMatch: 'full' },
  { path: 'espacios', component: EspaciosComponent },
  { path: 'espacio/:id/permisos', component: EspacioPermisosComponent },
  { path: 'espacio/:id/horarios', component: EspacioHorariosComponent },
  { path: 'espacio/:id/ingresos', component: EspacioIngresosComponent },
  { path: 'login', loadChildren: () => import('./auth/login/login.module').then(m => m.LoginModule) },
  { path: 'register', loadChildren: () => import('./auth/register/register.module').then(m => m.RegisterModule) },
  { path: 'home', loadChildren: () => import('./home/home.module').then(m => m.HomeModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
