export class Persona {
    id: number;
    nombre: string;
    apellidos: string;
    celular: string;
    email: string;
    macAddress: string;
  }