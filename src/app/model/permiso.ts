import { Persona } from './persona';
import { VinculoMomentoEspacio } from './vinculo-momento-espacio';

export class Permiso {
    id: number;
    activo: boolean;
    persona: Persona;
    vinculoMomentoEspacio: VinculoMomentoEspacio;
}