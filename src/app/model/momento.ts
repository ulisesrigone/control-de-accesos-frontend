import { Time } from '@angular/common';

export class Momento {
    id: number;
    diaDeSemana: number;
    horarioInicio: string;
    horarioFin: string;
    activo: boolean;
}