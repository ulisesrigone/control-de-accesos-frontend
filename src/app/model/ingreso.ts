import { Persona } from './persona';
import { Espacio } from './espacio';

export class Ingreso {
    id: number;
    fechaIngreso: Date;
    persona: Persona;
    espacio: Espacio;
}