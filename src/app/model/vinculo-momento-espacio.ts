import { Espacio } from './espacio';
import { Momento } from './momento';

export class VinculoMomentoEspacio {
    id: number;
    espacio: Espacio;
    momento: Momento;
    activo: boolean;
}
