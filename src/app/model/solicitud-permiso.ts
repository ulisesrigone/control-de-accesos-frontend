import { Persona } from './persona';
import { VinculoMomentoEspacio } from './vinculo-momento-espacio';
import { Permiso } from './permiso';

export class SolicitudPermiso {

    id: number;
    persona: Persona;
    vinculoMomentoEspacio: VinculoMomentoEspacio;
    aceptado: number;
    permisoPersona: Permiso;
    fechaSolicitud: Date;

}
