import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Permiso } from '../model/permiso';
import { SolicitudPermiso } from '../model/solicitud-permiso';

@Injectable({
  providedIn: 'root'
})
export class PermisoService {

  private baseUrl = 'http://localhost:8080/';
  
  constructor(private http: HttpClient) { }

  getPermisosByEspacio(idEspacio: number): Observable<any> {
    return this.http.get(`${this.baseUrl}espacio/${idEspacio}/permisos/`);
  }

  public crearPermiso(permiso: Permiso) {
    return this.http.post<Permiso>(`${this.baseUrl}permisos/`, permiso);
  }

  getSolicitudesByEspacio(idEspacio: number): Observable<any> {
    return this.http.get(`${this.baseUrl}espacio/${idEspacio}/solicitudes/`);
  }

  updateSolicitudPermiso(solicitudPermiso: SolicitudPermiso): Observable<SolicitudPermiso> {
    return this.http.put<SolicitudPermiso>(`${this.baseUrl}solicitud/`, solicitudPermiso);
  }
  
}
