import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Espacio } from '../model/espacio';
import { Momento } from '../model/momento';
import { VinculoMomentoEspacio } from '../model/vinculo-momento-espacio';

@Injectable({
  providedIn: 'root'
})
export class EspacioService {

  private baseUrl = 'http://localhost:8080/espacios/';

  constructor(private http: HttpClient) { }

  getEspacio(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  updateEspacio(id: number, value: any): Observable<object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deleteEspacio(idEspacio: number): Observable<any> {
    return this.http.delete(`http://localhost:8080/espacio/${idEspacio}`, { responseType: 'text' });
  }

  getEspaciosList(): Observable<any> {
    return this.http.get(`${this.baseUrl}/`);
  }
  
  getHorariosByEspacio(idEspacio: number): Observable<any> {
    return this.http.get(`http://localhost:8080/espacio/${idEspacio}/horarios/`);
  }

  getIngresosByEspacio(idEspacio: number): Observable<any> {
    return this.http.get(`http://localhost:8080/espacio/${idEspacio}/ingresos/`);
  }
  
  public findAll(): Observable<Espacio[]> {
    return this.http.get<Espacio[]>(this.baseUrl);
  }

  public crearEspacio(espacio: Espacio) {
    return this.http.post<Espacio>(this.baseUrl, espacio);
  }

  public crearHorario(idEspacio: number, horario: Momento) {
    return this.http.post<Momento>(`http://localhost:8080/espacio/${idEspacio}/horarios/`, horario);
  }

  public updateHorarioSemanal(horarioSemanal: VinculoMomentoEspacio): Observable<VinculoMomentoEspacio> {
    return this.http.put<VinculoMomentoEspacio>(`http://localhost:8080/horarios/`, horarioSemanal);
  }

}
