// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCejOql4sCWASfRa888FLCbuJ0uKMu2j1Q",
    authDomain: "control-de-accesos-ea55a.firebaseapp.com",
    databaseURL: "https://control-de-accesos-ea55a.firebaseio.com",
    projectId: "control-de-accesos-ea55a",
    storageBucket: "control-de-accesos-ea55a.appspot.com",
    messagingSenderId: "647127568470",
    appId: "1:647127568470:web:ecec85ed732e6b998a2bf7"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
